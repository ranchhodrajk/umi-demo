export default [
  {
    path: '/',
    component: '@/pages/index',
    routes: [
      {
        name: 'home',
        path: '/',
        component: '@/pages/Home/',
      },
      {
        name: 'dashboard',
        path: '/dashBoard',
        component: '@/pages/Dashboard/',
        routes: [
          {
            name: 'user',
            path: '/dashBoard/user',
            component: '/src/Components/User',
          },
          {
            name: 'signUp',
            path: '/dashBoard/signUp',
            component: '/src/Components/SignUp',
          },
          {
            name: 'signUpUser',
            path: '/dashBoard/signUpUser',
            component: '/src/Components/SignUpUser',
          },
          {
            name: 'signIn',
            path: '/dashBoard/signIn',
            component: '/src/Components/SignIn',
          },
        ],
      },
      {
        name: 'aboutus',
        path: '/aboutUs',
        // redirect:'/dashboard',
        component: '@/pages/AboutUs/',
      },
      {
        name: 'contactus',
        path: '/contactUs',
        component: '@/pages/ContactUs/',
      },
      {
        component: '@/pages/404',
      },
    ],
  },
];
