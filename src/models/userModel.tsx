// const initialState = {
//     count: 0,
// }
export default {
  namespace: 'counter',
  state: {
    count: 0,
  },
  reducers: {
    increment(state: any) {
      return { ...state, count: state.count + 1 };
    },
    decrement(state: any) {
      return { ...state, count: state.count - 1 };
    },
    incrementByFive(state: any, payload: any) {
      console.log('payload', payload);
      return { ...state, count: state.count + payload?.incer };
    },
    decrementByFive(state: any, payload: any) {
      return { ...state, count: state.count - payload.incer };
    },
  },
};
