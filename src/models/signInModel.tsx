export default {
  namespace: 'signUp',
  state: {
    name: '',
    username: '',
    password: '',
    cpassword: '',
    remember: false,
    userData: [],
  },
  reducers: {
    getSignUpData(state: any, payload: any) {
      return {
        ...state,
        name: payload?.values?.name,
        username: payload?.values?.username,
        password: payload?.values?.password,
        cpassword: payload?.values?.cpassword,
        remember: payload?.values?.remember,
      };
    },
    userListData(state: any, payload: any) {
      return { ...state, userData: payload };
    },
  },
};
