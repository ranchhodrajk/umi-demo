import React from 'react';
import { Button } from 'antd';
import { connect } from 'umi';

const User = (props: any) => {
  const count = props.count;
  const dispatch = props.dispatch;

  const onClickAdd = () => {
    dispatch({ type: 'counter/increment' });
  };

  const onClickRemove = () => {
    dispatch({ type: 'counter/decrement' });
  };

  const onClickAddByFive = (incer: number) => {
    dispatch({ type: 'counter/incrementByFive', incer });
  };

  const onClickRemoveByFive = (incer: number) => {
    dispatch({ type: 'counter/decrementByFive', incer });
  };

  return (
    <div>
      <div style={{ paddingLeft: '250px', paddingBottom: '20px' }}>{count}</div>
      <div>
        <Button type="primary" onClick={onClickAdd}>
          Increment
        </Button>
        <Button
          type="primary"
          style={{ marginLeft: '20px' }}
          onClick={onClickRemove}
          disabled={count <= 0 ? true : false}
        >
          Decrement
        </Button>
        <Button
          type="primary"
          style={{ marginLeft: '20px' }}
          onClick={() => onClickAddByFive(5)}
        >
          Increment By 5
        </Button>
        <Button
          type="primary"
          style={{ marginLeft: '20px' }}
          onClick={() => onClickRemoveByFive(5)}
          disabled={count <= 4 ? true : false}
        >
          Decrement By 5
        </Button>
      </div>
    </div>
  );
};

// export default User;

const mapStateToProps = (state: any) => ({
  count: state.counter.count,
});

export default connect(mapStateToProps)(User);
