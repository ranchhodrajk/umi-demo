import Dashboard from '@/pages/Dashboard';
import React, { useState } from 'react';
import { connect } from 'umi';
import { Card, Button, Row, Col, Empty } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';

const Documentation = (props: any) => {
  console.log('doc props', props.userData);

  return (
    <div>
      <Row gutter={[20, 20]}>
        {props?.userData?.userData?.length === 0 ? (
          <Row justify="center">
            <Col style={{ paddingLeft: '450px', paddingTop: '160px' }}>
              <Empty />
            </Col>
          </Row>
        ) : (
          props?.userData?.userData?.map((item: any) => (
            <Col md={6}>
              <Card
                size="small"
                title={
                  <Row justify="center">
                    <Col>{item?.name}</Col>
                  </Row>
                }
              >
                <p>
                  <b>Username</b>:{item?.username}
                </p>
                <p>
                  <b>Password</b>:{item?.password}
                </p>
                <p>
                  {item?.remember ? 'Password remember' : 'Password unremember'}
                </p>
              </Card>
            </Col>
          ))
        )}
      </Row>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  userData: state.signUp.userData,
});

export default connect(mapStateToProps)(Documentation);
