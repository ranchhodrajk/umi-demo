import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Checkbox, notification } from 'antd';
import { connect } from 'umi';
import { history } from 'umi';

const signIn = (props: any) => {
  interface UA {
    name: string;
    username: string;
    password: string;
    cpassword: string;
    remember: boolean;
  }

  const [userData, setuserData] = useState<UA[]>(
    props?.userData?.userData ? props?.userData?.userData : [],
  );

  useEffect(() => {
    console.log('userData', userData);
  }, [userData]);

  const onFinish = (values: any) => {
    console.log('values:', values);

    const indx = userData.findIndex(
      (item) => item.username === values.username,
    );
    if (indx < 0) {
      notification.error({
        message: 'Invalid credentials',
        description: 'Please try again',
      });
    } else {
      if (
        values.username === userData[indx]?.username &&
        values.password === userData[indx]?.password
      ) {
        notification.success({
          message: 'Login successful',
          description: `Wellcome ${userData[indx]?.name}`,
        });
        history.push('/');
      } else {
        notification.error({
          message: 'Invalid credentials',
          description: 'Please try again',
        });
      }
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <div>
        <Form
          name="basic"
          labelCol={{ offset: 8, span: 8 }}
          wrapperCol={{ offset: 8, span: 8 }}
          layout="vertical"
          initialValues={{ remember: true }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[{ required: true, message: 'Please input your username!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: 'Please input your password!' }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{ offset: 8, span: 8 }}
          >
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item wrapperCol={{ offset: 8, span: 8 }}>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};

const mapStateToProps = (state: any) => ({
  userData: state.signUp.userData,
});

export default connect(mapStateToProps)(signIn);
