import React, { useState, useEffect } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import { connect } from 'umi';

const SignUp = (props: any) => {
  const [form] = Form.useForm();
  interface UA {
    name: string;
    username: string;
    password: string;
    cpassword: string;
    remember: boolean;
  }

  const dispatch = props.dispatch;

  console.log('props.userData', props?.userData?.userData);

  const [userData, setuserData] = useState<UA[]>(
    props?.userData?.userData ? props?.userData?.userData : [],
  );

  const onFinish = (values: any) => {
    dispatch({ type: 'signUp/getSignUpData', values });

    const obj = {
      name: values.name,
      username: values.username,
      password: values.password,
      cpassword: values.cpassword,
      remember: values.remember,
    };

    const temp = [...userData];
    temp.push(obj);
    setuserData(temp);
  };

  useEffect(() => {
    dispatch({ type: 'signUp/userListData', userData });
  }, [userData]);

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <div>
      <Form
        name="basic"
        labelCol={{ offset: 8, span: 8 }}
        wrapperCol={{ offset: 8, span: 8 }}
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          label="Name"
          name="name"
          rules={[{ required: true, message: 'Please input your name!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Username"
          name="username"
          rules={[{ required: true, message: 'Please input your username!' }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Password"
          name="password"
          rules={[{ required: true, message: 'Please input your password!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          label="Confirm Password"
          name="cpassword"
          rules={[
            { required: true, message: 'Please input your confrim password!' },
          ]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item
          name="remember"
          valuePropName="checked"
          wrapperCol={{ offset: 8, span: 8 }}
        >
          <Checkbox>Remember me</Checkbox>
        </Form.Item>

        <Form.Item wrapperCol={{ offset: 8, span: 8 }}>
          <Button type="primary" htmlType="submit">
            Submit
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

// export default Assets;
const mapStateToProps = (state: any) => ({
  name: state.signUp.name,
  username: state.signUp.username,
  password: state.signUp.password,
  cpassword: state.signUp.cpassword,
  remember: state.signUp.remember,
  userData: state.signUp.userData,
});

export default connect(mapStateToProps)(SignUp);
