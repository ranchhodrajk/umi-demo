import React from 'react';
import './Home.scss';

import { Row, Col, Button } from 'antd';

const Home = () => {
  return (
    <div className="wrap-container">
      <Row justify="center">
        <Col md={14}>
          <h1>Wellcome to Home page</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae
            voluptates iure ad quisquam eum quam dolorum nihil voluptatum?
            Aliquam sapiente blanditiis, voluptates nemo dolore eum velit vero
            at et quam.
          </p>
        </Col>
      </Row>
    </div>
  );
};

export default Home;
