import React, { useState, useEffect } from 'react';
import './index.scss';
import { Layout, Menu } from 'antd';
const { Header, Content, Footer, Sider } = Layout;
import { Link } from 'umi';

export default function IndexPage(props: { children: React.ReactNode }) {
  interface Key {
    key: string;
  }

  const { children } = props;

  const [current, setCurrent] = useState<string>('');

  useEffect(() => {
    setCurrent(location.pathname);
  });

  const handleClick = (text: string) => {
    setCurrent(text);
  };

  return (
    <>
      <Layout>
        <Header className="header">
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            onClick={(e: Key) => handleClick(e.key)}
            selectedKeys={[current]}
          >
            <Menu.Item key="/">
              <Link to="/">Home</Link>
            </Menu.Item>
            <Menu.Item key="/dashboard">
              <Link to="/dashboard">Dashboard</Link>
            </Menu.Item>
            <Menu.Item key="/aboutUs">
              <Link to="/aboutUs">About Us</Link>
            </Menu.Item>
            <Menu.Item key="/contactUs">
              <Link to="/contactUs">Contact Us</Link>
            </Menu.Item>
          </Menu>
        </Header>
        <Content className="content-physican">{children}</Content>
      </Layout>
    </>
  );
}
