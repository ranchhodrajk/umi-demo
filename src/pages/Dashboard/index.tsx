import React, { useEffect, useState } from 'react';
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { Layout, Menu } from 'antd';
import type { MenuProps } from 'antd';
import { history } from 'umi';

const { Header, Content, Footer, Sider } = Layout;
const { SubMenu } = Menu;

const Dashboard = (props: { children: React.ReactNode }) => {
  const { children } = props;

  const [openKeys, setOpenKeys] = useState([]);

  useEffect(() => {
    history.push(`/dashBoard/user`);
  }, []);

  const menuItem = (item: any) => {
    history.push(`/dashBoard/${item?.key}`);
  };

  return (
    <Layout>
      <Sider width={200} className="site-layout-background">
        <Menu
          theme="dark"
          openKeys={openKeys}
          defaultSelectedKeys={['1']}
          mode="inline"
        >
          <Menu.Item key="user" icon={<PieChartOutlined />} onClick={menuItem}>
            Simple DVA
          </Menu.Item>
          <Menu.Item key="signUp" icon={<DesktopOutlined />} onClick={menuItem}>
            Sign Up
          </Menu.Item>
          <Menu.Item
            key="signUpUser"
            icon={<FileOutlined />}
            onClick={menuItem}
          >
            Sign Up User
          </Menu.Item>
          <Menu.Item key="signIn" icon={<DesktopOutlined />} onClick={menuItem}>
            Sign In
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout style={{ padding: '0 24px 24px' }}>
        <Content
          className="site-layout-background"
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default Dashboard;
