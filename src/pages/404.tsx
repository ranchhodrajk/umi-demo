import React from 'react';
import { Result, Button } from 'antd';
import { history } from 'umi';
const Page404 = () => {
  const onClickBackHome = () => {
    history.goBack();
  };

  return (
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={
        <Button type="primary" onClick={onClickBackHome}>
          Go Home
        </Button>
      }
    />
  );
};

export default Page404;
