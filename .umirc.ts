import { defineConfig } from 'umi';
import routers from './config/routers';

const fs = require('fs');
const path = require('path');
const lessToJS = require('less-vars-to-js');
const themeVariables = lessToJS(
  fs.readFileSync(path.resolve(__dirname, './ant-theme-vars.less'), 'utf8'),
  { resolveVariables: true, stripPrefix: true },
);

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  // layout: {},
  routes: routers,
  fastRefresh: {},
  antd: {},
  webpack5: {},
  // ssr: {},
  theme: themeVariables,
  dva: {},
});
